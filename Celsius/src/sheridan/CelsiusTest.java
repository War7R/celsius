package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testConvertFromFahrenheit() {
		assertTrue("Incorrect value for celsuis",
				Celsius.convertFromFahrenheit(32) == 0);
	}

	@Test 
	public void testConvertFromFahrenheitNegative(){
		assertFalse("Incorrect value for celsuis",
				Celsius.convertFromFahrenheit(0) == 0);
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryIn(){
		assertTrue("Incorrect value for celsuis",
				Celsius.convertFromFahrenheit(6) == -14);
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryOut(){
		assertFalse("Incorrect value for celsuis",
				Celsius.convertFromFahrenheit(7) == -13);
	}

}
